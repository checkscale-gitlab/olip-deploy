#!/bin/bash

while true; do
    bat_level=$(echo "get battery" | nc -q 0 127.0.0.1 8423 | cut -d ":" -f2)
    bat_v=$(echo "get battery_v" | nc -q 0 127.0.0.1 8423 | cut -d ":" -f2)
    bat_temp=$(echo "get temperature" | nc -q 0 127.0.0.1 8423 | cut -d ":" -f2)
    battery_charing=$(echo "get battery_power_plugged" | nc -q 0 127.0.0.1 8423 | cut -d ":" -f2)
    cpu_temp=$(sensors -j | jq -r ".[].temp1" | grep temp1 | cut -d ":" -f2)
    time=$(date '+%H:%M:%S')
    date=$(date '+%F')
    load1=$(cat /proc/loadavg | cut -d " " -f1)
    load2=$(cat /proc/loadavg | cut -d " " -f2)
    load3=$(cat /proc/loadavg | cut -d " " -f3)
    con_wifi_ap=$(iw dev wlan0 station dump |grep Station | wc -l)
    echo $date,$time,$load1,$load2,$load3,$bat_temp,$cpu_temp,$bat_level,$bat_v,$battery_charing,$con_wifi_ap >> /data/logs/ideascube_mon.log
    sleep 60
done
