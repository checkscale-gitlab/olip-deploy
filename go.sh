#!/bin/bash

ANSIBLECAP_PATH="/var/lib/ansible/local"
GIT_REPO_URL="https://gitlab.com/bibliosansfrontieres/olip/olip-deploy.git"
ANSIBLE_BIN="/usr/bin/ansible-pull"
ANSIBLE_LOGS="/var/log/ansible-pull.log"
ANSIBLE_ETC="/etc/ansible/facts.d/"
BRANCH="master"
GIT_RELEASE_TAG="1.9.4"
STORAGE=""

function die()
{
    >&2 echo "FATAL: $*"
    exit 1
}

[ $EUID -eq 0 ] || die "You have to be root to run this script."

function internet_check()
{
    echo -e "[+] Check Internet connection... "
    if [[ ! `ping -q -c 2 github.com` ]]
    then
        die "Repository is unreachable, check your Internet connection."
    fi
    echo "Done."
}

function install_ansible()
{
    internet_check
    echo -e "[+] Install PPA... "
    apt update
    apt install --yes --upgrade software-properties-common ca-certificates
    apt-add-repository --yes --update ppa:ansible/ansible
    echo -e "[+] Update Ansible... "
    apt install -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --quiet --quiet -y ansible=2.9* git
    echo 'Done.'
}

function clone_ansiblecube()
{
    echo -e "[+] Checking for internet connectivity... "
    internet_check
    echo 'Done.'

    echo -e "[+] Clone ansiblecap repo... "
    mkdir --mode 0755 -p ${ANSIBLECAP_PATH}
    cd ${ANSIBLECAP_PATH}/../
    git clone ${GIT_REPO_URL} local

    mkdir --mode 0755 -p ${ANSIBLE_ETC}
    cp ${ANSIBLECAP_PATH}/hosts /etc/ansible/hosts
    echo 'Done.'
}

[ -x ${ANSIBLE_BIN} ] || install_ansible || die "Failed to install Ansible."
[ -d ${ANSIBLECAP_PATH} ] || clone_ansiblecube || die "Failed to clone the olip-deploy repository."

echo -e "Checking file access" >> $ANSIBLE_LOGS
[ $? -ne 0 ] && die "No space left to write logs or permission problem."

while [[ $# -gt 0 ]]
do
    case $1 in

        -n|--name)

            if [ -z "$2" ]
            then
                echo -e "\n\t[+] ERROR\n\t--name : Missing server name\n"

                exit 0;
            fi
            SERVER_NAME=$2

        shift
        ;;

        -u|--url)

            if [ -z "$2" ]
            then
                echo -e "\n\t[+] ERROR\n\t--url : Missing domain url\n"

                exit 0;
            fi
            DOMAIN=$2

        shift
        ;;

        -d|--descriptor)

            if [ -z "$2" ]
            then
                echo -e "\n\t[+] ERROR\n\t--descriptor : Missing descriptor file\n"

                exit 0;
            fi
            DESCRIPTOR_FILE=$2

        shift
        ;;

        -t|--tags)

            if [ -n "$2" ]
            then
                TAGS="--tags $2"
            fi

        shift
        ;;

        -e|--extra-vars)

            if [ -n "$2" ]
            then
                EXTRA_VARS_EXTRA=$2
            fi

        shift
        ;;

        *)
            help
        ;;
    esac
    shift
done

cd $ANSIBLECAP_PATH

echo -e "[+] Start configuration... tail -f $ANSIBLE_LOGS for logs"

$ANSIBLE_BIN \
	--purge \
	-C $BRANCH \
	-d $ANSIBLECAP_PATH \
	-i hosts \
	-U $GIT_REPO_URL \
	main.yml \
	--extra-vars "end_user_server_name=$SERVER_NAME end_user_domain_name=$DOMAIN end_user_olip_file_descriptor=$DESCRIPTOR_FILE $EXTRA_VARS_EXTRA" \
	$TAGS
